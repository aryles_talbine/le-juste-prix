let input =document.getElementById("prix");
let formulaire=document.querySelector("#formulaire");
let error=document.querySelector('small');
let coup=0;
let nombreChoisi;
error.style.display="none"

function verifierNombre(nbr){
    let instruction = document.createElement('div');

    if(nbr < nombre){
        instruction.textContent ="#" + coup + "(" + nbr + ") C'est plus ! ";
        instruction.className = "instruction plus"  
    }
    else if(nbr > nombre){
        instruction.textContent ="#" + coup + "(" + nbr + ") C'est moins ! ";
        instruction.className = "instruction moins"  
    }
    else {
        instruction.textContent ="#" + coup + "(" + nbr + ")  Félicitation ! ";
        instruction.className = "instruction fini"
        input.disabled="true"  
    }
    document.querySelector("#instructions").prepend(instruction);
}

function generateAleatoir(max){
    return Math.floor(Math.random()* Math.floor(max))
}

let nombre =generateAleatoir(1001);

input.addEventListener('keyup',()=>{
if(isNaN(input.value)){
    error.style.display="inline";
}
else {
    error.style.display="none"
}
});

formulaire.addEventListener("submit",(e)=>{

    e.preventDefault();
    if(isNaN(input.value) || input.value ==undefined || input.value ==null || input.value=='' ){
        input.style.borderColor="red"
    }
    else
    {
        coup++;
        input.style.borderColor="silver";
        nombreChoisi=input.value;
        input.value='';
        verifierNombre(nombreChoisi);
    }
});

console.log(nombre)